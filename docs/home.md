---
title: Seedbox.io Legacy Documentation
slug: /
sidebar_label: Home
---

:::caution
This is only for the **legacy Seedbox.io Shared and Storage slots**
:::

## Introduction

On the left the different ranges are sperated in different categories. Since the platforms are based on different systems.
Do you need more help then please open a [Ticket](https://my.hostingby.design/clientarea.php)
